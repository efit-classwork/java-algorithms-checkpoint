package com.galvanize;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {
    @Test
    public void confirmThatAStringCanBeCheckedForAllRepeatingLetters(){
        Algorithm algorithm = new Algorithm();
        boolean shouldBeTrue = algorithm.allEqual("aAa");
        boolean shouldBeFalse = algorithm.allEqual("bbBbabbb");
        boolean shouldBeFalse2 = algorithm.allEqual("");
        assertTrue(shouldBeTrue);
        assertFalse(shouldBeFalse);
        assertFalse(shouldBeFalse2);
    }
    @Test
    public void confirmThatCharactersCanBeCountedForEachUnique(){
        Algorithm algorithm = new Algorithm();
        Map<String, Long> testMap = Map.of("a", 2L);
        Map<String, Long> testMap2 = Map.of("a",1L,
                "b",2L, "c",1L, "d",1L);
        Map<String, Long> testMap3 = new HashMap<>();
        Map<String, Long> a2 = algorithm.letterCount("aa");
        Map<String, Long> aB2CD = algorithm.letterCount("abBcd");
        Map<String, Long> empty = algorithm.letterCount("");
        assertEquals(testMap,a2);
        assertEquals(testMap2,aB2CD);
        assertEquals(testMap3,empty);
    }
    @Test
    public void confirmThatTwoArraysCanBeCombinedStringByString(){
        Algorithm algorithm = new Algorithm();
        String  adbecf = algorithm.interleave(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));
        algorithm.interleave(Arrays.asList("a", "c", "e"), Arrays.asList("b", "d", "f"));
        algorithm.interleave(Collections.emptyList(), Collections.emptyList());
    }
}