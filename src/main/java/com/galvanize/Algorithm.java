package com.galvanize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {
    public boolean allEqual(String input){
        boolean result = true;
        if(input == null || input == ""){
            result = false;
            return result;
        }
        String tempInput = input.toLowerCase();
        for (int i = 0; i < tempInput.length(); i++) {
            if(tempInput.charAt(0) != tempInput.charAt(i)){
                result = false;
                return result;
            }
        }
        return result;
    }
    public Map<String,Long> letterCount(String input){
        Map<String,Long> result = new HashMap<>();
        if(input == null || input == ""){
            return result;
        }
        String tempInput = input.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            char hold = tempInput.charAt(i);
            long count = 0;
            if(!result.containsKey(Character.toString(hold))){
                for (int j = 0; j < input.length(); j++) {
                    if(tempInput.charAt(j) == hold){
                        count++;
                    }
                    result.put(Character.toString(hold),count);
                }
            }
        }
        return result;
    }
    public String interleave(List<String> string1, List<String> string2){
        StringBuilder accumulatorString = new StringBuilder();
        if(string1.size() == 0 || string2.size() == 0)
            return "";
        for (int i = 0; i < string1.size(); i++) {
            accumulatorString.append(string1.get(i));
            accumulatorString.append(string2.get(i));
        }
        String result = accumulatorString.toString();
        return result;
    }
}
